# PARA RUBBER
## Caractères en Caoutchouc / Para Rubber Type Made in Belgium



Dessin typographique librement inspiré de caractères en caoutchouc Para Rubber Type, trouvés dans une boite en métal à Bruxelles. Origine Belge, date et nom inconnus. Si quelqu'un a des pistes, à me le faire savoir.

Copyright (c) 2019, Leonard Mabille, <leomabille@gmail.com>, <www.luuse.io>, <www.typotheque.luuse.io>

![img/All.png](img/All.png)
![img/2.png](img/2.png)
![img/a.png](img/a.png)
![img/G.png](img/G.png)
![img/g.png](img/g.png)
![img/k.png](img/k.png)
![img/Q.png](img/Q.png)

![img/2019-07-23_19-21-52.png](img/2019-07-23_19-21-52.png)
![img/cap.png](img/cap.png)
![img/bdc.png](img/bdc.png)

